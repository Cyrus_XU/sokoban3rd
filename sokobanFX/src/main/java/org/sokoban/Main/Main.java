package org.sokoban.Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.sokoban.App;
import org.sokoban.GameControl.GameInitializer;
import org.sokoban.GameControl.KeyHandler;
import org.sokoban.GameControl.LevelController;
import org.sokoban.GameControl.MenuController;
import org.sokoban.GameView.Music;

import java.io.File;
import java.io.IOException;


public class Main extends Application {
    private Music music = new org.sokoban.GameView.Music();
    private Stage primaryStage;
    private KeyHandler keyHandler;
    private LevelController levelController;
    private GameInitializer gameInitializer;
    private GridPane gameGrid;
    private File saveFile;

    public boolean debug = false;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        gameGrid = new GridPane();
        gameInitializer = new GameInitializer(gameGrid, primaryStage);
        MenuController controller = new MenuController(gameInitializer);
        keyHandler = gameInitializer.getKeyHandler();
        levelController = gameInitializer.getLevelController();

        GridPane root = new GridPane();
        root.add(loadFXML("Menu", controller), 0, 0);
        root.add(gameGrid, 0, 1);
        primaryStage.setTitle(LevelController.GAME_NAME);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
//        loadDefaultSaveFile(primaryStage);
    }

    /**
     *
     * @param fxml
     * @param controller
     * @return
     * @throws IOException
     */
    private static Parent loadFXML(String fxml, MenuController controller) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        fxmlLoader.setController(controller);
        return fxmlLoader.load();
    }
}
