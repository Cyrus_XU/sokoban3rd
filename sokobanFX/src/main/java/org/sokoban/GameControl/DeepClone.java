package org.sokoban.GameControl;

import org.sokoban.GameModel.Level;

import java.io.*;
import java.util.List;

public class DeepClone {


    /**
     * deep clone a list of levels
     * @param levels
     * @return clonedLevels
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static <Level> List<Level> deepCopyLevels(List<Level> levels) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(levels);

        ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
        ObjectInputStream in = new ObjectInputStream(byteIn);
        @SuppressWarnings("unchecked")
        List<Level> clonedLevels = (List<Level>) in.readObject();
        return clonedLevels;
    }

    /**
     * deep clone a single value
     * @param level
     * @return clonedLevel
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Level deepGetLevel(Level level) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(level);

        ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
        ObjectInputStream in = new ObjectInputStream(byteIn);
        @SuppressWarnings("unchecked")
        Level clonedLevel = (Level) in.readObject();
        return clonedLevel;
    }
}
