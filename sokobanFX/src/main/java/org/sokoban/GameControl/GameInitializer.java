package org.sokoban.GameControl;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.effect.Effect;
import javafx.scene.effect.MotionBlur;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.sokoban.GameModel.Level;
import org.sokoban.GameView.GameObject;
import org.sokoban.GameView.GraphicObject;
import org.sokoban.GameView.GraphicObjectImg;
import org.sokoban.GameView.Music;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import static org.sokoban.GameControl.LevelController.logger;

public class GameInitializer {
    private Music music = new org.sokoban.GameView.Music();
    private Stage primaryStage;
    private KeyHandler keyHandler;
    private LevelController levelController;
    private GridPane gameGrid;
    private File saveFile;
    public int movesCount = 0;

    public KeyHandler getKeyHandler() {
        return keyHandler;
    }
    public LevelController getLevelController() {
        return levelController;
    }
    public GridPane getGameGrid() {
        return this.gameGrid;
    }

    /**
     * Method to initialize the game by loading default save file.
     * @param gameGrid
     * @param primaryStage
     */
    public GameInitializer(GridPane gameGrid, Stage primaryStage){
        this.gameGrid = gameGrid;
        loadDefaultSaveFile(primaryStage);
    }

    /**
     * Method to
     * @param primaryStage
     */
    void loadDefaultSaveFile(Stage primaryStage) {
        this.primaryStage = primaryStage;
        InputStream in = getClass().getClassLoader().getResourceAsStream("level/SampleGame.skb");
        initializeGame(in);
        setEventFilter();
    }

    /**
     *
     * @param input
     */
    private void initializeGame(InputStream input) {
        levelController = new LevelController(input, true);
        keyHandler = new KeyHandler(levelController);
        reloadGrid();
    }

    /**
     *
     */
    private void setEventFilter() {
        primaryStage.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
//            gameEngine.handleKey(event.getCode());
            keyHandler.handleKey(event.getCode());
            reloadGrid();
        });}

    /**
     *
     * @throws FileNotFoundException
     */
    public void loadGameFile() throws FileNotFoundException {
        this.levelController.steps = new ArrayList<>(5);

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Save File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Sokoban save file", "*.skb"));
        saveFile = fileChooser.showOpenDialog(primaryStage);

        if (saveFile != null) {
            if (levelController.isDebugActive()) {
                logger.info("Loading save file: " + saveFile.getName());
            }
            initializeGame(new FileInputStream(saveFile));
        }}

    /**
     *
     */
    public void reloadGrid() {
        if (levelController.isGameComplete()) {
            showVictoryMessage();
            return;
        }

        Level currentLevel = levelController.getCurrentLevel();
        Level.LevelIterator levelGridIterator = (Level.LevelIterator) currentLevel.iterator();
        gameGrid.getChildren().clear();
        while (levelGridIterator.hasNext()) {
            addObjectToGrid(levelGridIterator.next(), levelGridIterator.getcurrentposition());
        }gameGrid.autosize();
        primaryStage.sizeToScene();
    }

    /**
     *
     */
    private void showVictoryMessage() {
        String dialogTitle = "Game Over!";
        String dialogMessage = "You completed " + levelController.mapSetName + " in " + keyHandler.movesCount + " moves!";
        MotionBlur mb = new MotionBlur(2, 3);

        newDialog(dialogTitle, dialogMessage, mb);
    }

    /**
     *
     * @param dialogTitle
     * @param dialogMessage
     * @param dialogMessageEffect
     */
    public void newDialog(String dialogTitle, String dialogMessage, Effect dialogMessageEffect) {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        dialog.setResizable(false);
        dialog.setTitle(dialogTitle);

        Text text1 = new Text(dialogMessage);
        text1.setTextAlignment(TextAlignment.CENTER);
        text1.setFont(javafx.scene.text.Font.font(14));

        if (dialogMessageEffect != null) {
            text1.setEffect(dialogMessageEffect);
        }

        VBox dialogVbox = new VBox(20);
        dialogVbox.setAlignment(Pos.CENTER);
        dialogVbox.setBackground(Background.EMPTY);
        dialogVbox.getChildren().add(text1);

        Scene dialogScene = new Scene(dialogVbox, 350, 150);
        dialog.setScene(dialogScene);
        dialog.show();
    }

    /**
     *
     * @param gameObject
     * @param location
     */
    private void addObjectToGrid(GameObject gameObject, Point location) {
        GraphicObjectImg graphicObject = new GraphicObjectImg(gameObject);
        gameGrid.add(graphicObject, location.y, location.x);
    }
}
