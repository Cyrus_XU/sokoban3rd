package org.sokoban.GameView;

import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
import org.sokoban.GameControl.LevelController;

public class GraphicObjectImg extends ImageView {
    /**
     * This class generates the graphic objects to display,
     * @param obj
     */
    public GraphicObjectImg(GameObject obj) {
        String imgUrl = null;

        switch (obj) {
            case WALL:
                imgUrl = "img/wall.png";
                break;

            case CRATE:
                imgUrl = "img/crate.png";
                break;

            case DIAMOND:
                imgUrl = "img/diamond.png";

                if (LevelController.isDebugActive()) {
                    FadeTransition ft = new FadeTransition(Duration.millis(1000), this);
                    ft.setFromValue(1.0);
                    ft.setToValue(0.2);
                    ft.setCycleCount(Timeline.INDEFINITE);
                    ft.setAutoReverse(true);
                    ft.play();
                }
                break;

            case KEEPER:
                imgUrl = "img/character.png";
                break;

            case FLOOR:
                imgUrl = "img/floor.png";
                break;

            case CRATE_ON_DIAMOND:
                imgUrl = "img/crate_on_diamond.png";
                break;

            default:
                String message = "Error in Level constructor. Object not recognized.";
                LevelController.logger.severe(message);
                throw new AssertionError(message);
        }

        Image img = new Image(String.valueOf(getClass().getClassLoader().getResource(imgUrl)));
        this.setImage(img);
        this.setFitHeight(30);
        this.setFitWidth(30);

        if (LevelController.isDebugActive()) {
            this.setStyle("-fx-border-radius: 0.25;");
        }
    }
}
