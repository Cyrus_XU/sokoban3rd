package org.sokoban.GameView;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * @View
 * Music is UI element
 */
public class Music {
    public MediaPlayer bgm;

    public Music(){
        String bgmPath = this.getClass().getClassLoader().getResource("music/puzzle_theme.wav").toString();
        Media bgmMedia = new Media(bgmPath);
        this.bgm = new MediaPlayer(bgmMedia);
        this.bgm.setOnEndOfMedia(new Runnable() {
            @Override public void run() {
                bgm.play();
            }
        });
    }
}
