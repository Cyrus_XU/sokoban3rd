package org.sokoban.GameControl;

import java.io.*;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.effect.Effect;
import javafx.scene.layout.GridPane;
import org.sokoban.App;
import org.sokoban.GameModel.Level;
import org.sokoban.GameView.Music;

import static org.sokoban.GameControl.LevelController.isDebugActive;
import static org.sokoban.GameControl.LevelController.logger;

public class MenuController {
    private Music music = new org.sokoban.GameView.Music();
    private LevelController levelController;
    private KeyHandler keyHandler;
    private GridPane gameGrid;
    private GameInitializer gameInitializer;
    private boolean isToggled = false;

    /**
     * MenuController initializer.
     * @param gameInitializer
     */
    public MenuController(GameInitializer gameInitializer){
        this.gameInitializer = gameInitializer;
        this.levelController = gameInitializer.getLevelController();
        this.keyHandler = gameInitializer.getKeyHandler();
        this.gameGrid = gameInitializer.getGameGrid();
    }

    public void setLevelController(LevelController levelController) {
        this.levelController = levelController;
    }

    /**
     *
     * @throws FileNotFoundException
     */
    private void loadGameFile() throws FileNotFoundException {
        gameInitializer.loadGameFile();
        levelController = gameInitializer.getLevelController();
    }
    private void reloadGrid() {
        gameInitializer.reloadGrid();
    }
    private void newDialog(String dialogTitle, String dialogMessage, Effect dialogMessageEffect) {
        gameInitializer.newDialog(dialogTitle, dialogMessage, dialogMessageEffect);
    }

    @FXML
    public void closeGame() {
        System.exit(0);
    }

    public void saveGame() throws IOException {
        levelController.saveGame();
    }
    @FXML
    public void loadGame() {
        try {
            loadGameFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void undo() {
        int numberOfElements = levelController.steps.size();
        if (numberOfElements <= 0){
            Alert information = new Alert(Alert.AlertType.INFORMATION,"You have reach the start point of this level.");
            information.setTitle("Undo Failed");
            information.setHeaderText("There is no record of movement!");
            information.showAndWait();
        } else {
            Level undoLevel = levelController.steps.get(numberOfElements - 1);
            levelController.steps.remove(numberOfElements - 1);
            levelController.undo(undoLevel);
        }
        reloadGrid();
    }

    @FXML
    public void resetLevel() {//implement resetLevel
        levelController.resetCurrentLevel();
        reloadGrid();
    }
    @FXML
    public void showAbout() {
        String title = "About this game";
        String message = "Game created by Chen XU\n";

        newDialog(title, message, null);
    }
    @FXML
    public void toggleMusic(ActionEvent e) {
        isToggled = !isToggled;
        System.out.println("toggleMusic"+isToggled);
        if (isToggled){
            music.bgm.play();
        }else {
            music.bgm.stop();
        }
    }
    @FXML
    public void toggleDebug() {
        levelController.toggleDebug();
        reloadGrid();
    }
}