package org.sokoban.GameControl;

import javafx.scene.input.KeyCode;
import org.sokoban.GameModel.Level;
import org.sokoban.GameView.GameObject;

import java.awt.*;
import java.io.IOException;

import static org.sokoban.GameControl.DeepClone.deepGetLevel;

// TODO: separate KeyHandler from GameEngine

public class KeyHandler {
    public LevelController LevelController;
    public int movesCount = 0;
    /**
     *
     */
    public KeyHandler(LevelController LevelController){
        this.LevelController = LevelController;
    }
    public void setMovesCount(int offset){
        this.movesCount += offset;
    }

    /**
     *
     * @param code
     */
    public void handleKey(KeyCode code) {
        switch (code) {
            case UP:
                move(new Point(-1, 0));
                break;

            case RIGHT:
                move(new Point(0, 1));
                break;

            case DOWN:
                move(new Point(1, 0));
                break;

            case LEFT:
                move(new Point(0, -1));
                break;
            default:
                // TODO: implement something funny.
        }

        if (LevelController.isDebugActive()) {
            System.out.println(code);
        }
    }

    /**
     *
     * @param delta
     */
    public void move(Point delta) {
        Level currentLevel = LevelController.getCurrentLevel();
        recordSteps(currentLevel);

        if (LevelController.isGameComplete()) {
            return;
        }

        Point keeperPosition = currentLevel.getKeeperPosition();
        GameObject keeper = currentLevel.objectsGrid.getGameObjectAt(keeperPosition);
        Point targetObjectPoint = GameGrid.translatePoint(keeperPosition, delta);
        GameObject keeperTarget = currentLevel.objectsGrid.getGameObjectAt(targetObjectPoint);

        boolean keeperMoved = false;

        switch (keeperTarget) {

            case WALL:
                break;

            case CRATE:

                GameObject crateTarget = currentLevel.getTargetObject(targetObjectPoint, delta);
                if (crateTarget != GameObject.FLOOR) {
                    break;
                }

                currentLevel.objectsGrid.putGameObjectAt(currentLevel.objectsGrid.getGameObjectAt(GameGrid.translatePoint(targetObjectPoint, delta)), targetObjectPoint);
                currentLevel.objectsGrid.putGameObjectAt(keeperTarget, GameGrid.translatePoint(targetObjectPoint, delta));
                currentLevel.objectsGrid.putGameObjectAt(currentLevel.objectsGrid.getGameObjectAt(GameGrid.translatePoint(keeperPosition, delta)), keeperPosition);
                currentLevel.objectsGrid.putGameObjectAt(keeper, GameGrid.translatePoint(keeperPosition, delta));
                keeperMoved = true;
                break;

            //FLOOR AND DIAMOND shares the same case
            case FLOOR:
            case DIAMOND:
                currentLevel.objectsGrid.putGameObjectAt(currentLevel.objectsGrid.getGameObjectAt(GameGrid.translatePoint(keeperPosition, delta)), keeperPosition);
                currentLevel.objectsGrid.putGameObjectAt(keeper, GameGrid.translatePoint(keeperPosition, delta));
                keeperMoved = true;
                break;

            default:
                LevelController.logger.severe("The object to be moved was not a recognised GameObject.");
                throw new AssertionError("This should not have happened. Report this problem to the developer.");
        }

        if (keeperMoved) {
            keeperPosition.translate((int) delta.getX(), (int) delta.getY());
            movesCount++;
            if (currentLevel.isComplete()) {
                if (LevelController.isDebugActive()) {
                    ClearRecord();
                    System.out.println("Level complete!");
                }
                currentLevel = LevelController.getNextLevel();
                LevelController.setCurrentLevel(currentLevel);
            }
        }

        if (LevelController.isDebugActive()) {
            //level index的记得删掉，这个在最后一关会报错NullPointer
            System.out.println("Current level index:" + currentLevel.getIndex());
            System.out.println("Current level name:" + currentLevel.getName());
            System.out.println("Current level state:");
            System.out.println(currentLevel.toString());
            System.out.println("Keeper pos: " + keeperPosition);
            System.out.println("Movement source obj: " + keeper);
            System.out.printf("Target object: %s at [%s]", keeperTarget, targetObjectPoint);
        }
    }

    /**
     * Store current level into a step history array.
     * @param currentLevel
     */
    private void recordSteps(Level currentLevel) {
        try {
            LevelController.steps.add(deepGetLevel(currentLevel));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    private void ClearRecord(){
        LevelController.steps.clear();
    }
}
