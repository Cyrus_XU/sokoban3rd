package org.sokoban.GameControl;

import org.sokoban.GameModel.GameLogger;
import org.sokoban.GameModel.Level;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.FileHandler;

import static org.sokoban.GameControl.DeepClone.deepCopyLevels;
import static org.sokoban.GameControl.DeepClone.deepGetLevel;


public class LevelController {
    public static final String GAME_NAME = "SokobanFX";
    public static GameLogger logger;
    public List<Level> steps = new ArrayList<>(1000);
    public String mapSetName;
    private static boolean debug = false;
    private Level currentLevel;
    private List<Level> levels;
    private List<Level> defaultLevels;
    private boolean gameComplete = false;

    public LevelController(InputStream input, boolean production) {
        try {
            logger = new GameLogger();
            levels = loadGameFile(input);
            defaultLevels = deepCopyLevels(levels);
            currentLevel = getNextLevel();
        } catch (IOException x) {
            System.out.println("Cannot create logger.");
        } catch (NoSuchElementException e) {
            logger.warning("Cannot load the default save file: " + e.getStackTrace());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @return currentLevel
     */
    public Level getCurrentLevel() {
        return currentLevel;
    }

    public List<Level> getLevels() {
        return levels;
    }

    public List<Level> getSteps() {
        return steps;
    }

    /**
     *
     * @param currentLevel - the new value to set
     */
    public void setCurrentLevel(Level currentLevel) {
        this.currentLevel = currentLevel;
    }

    public boolean isGameComplete() {
        return gameComplete;
    }
    public static boolean isDebugActive() {
        return debug;
    }
    public void toggleDebug() {
        debug = !debug;
    }

    protected List<Level> loadGameFile(InputStream input) {
        List<Level> levels = new ArrayList<>(5);
        int levelIndex = 0;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
            boolean parsedFirstLevel = false;
            List<String> rawLevel = new ArrayList<>();
            String levelName = "";

            while (true) {
                String line = reader.readLine();

                if (line == null) {
                    if (rawLevel.size() != 0) {
                        Level parsedLevel = new Level(levelName, ++levelIndex, rawLevel);
                        levels.add(parsedLevel);
                    }
                    break;
                }

                if (line.contains("MapSetName")) {
                    mapSetName = line.replace("MapSetName: ", "");
                    System.out.println("mapSetName is"+mapSetName);
                    continue;
                }
//                if (line.contains("MoveCount")) {
//                    movecount = line.replace("MoveCount: ", "");
//                    continue;
//                }
//                if (line.contains("Current level index")) {
//                    mapSetName = line.replace("Current level index: ", "");
//                    continue;
//                }

                if (line.contains("LevelName")) {
                    if (parsedFirstLevel) {
                        Level parsedLevel = new Level(levelName, ++levelIndex, rawLevel);
                        levels.add(parsedLevel);
                        rawLevel.clear();
                    } else {
                        parsedFirstLevel = true;
                    }

                    levelName = line.replace("LevelName: ", "");
                    continue;
                }

                line = line.trim();
                line = line.toUpperCase();
                if (line.matches(".*W.*W.*")) {
                    rawLevel.add(line);
                }
            }

        } catch (IOException e) {
            logger.severe("Error trying to load the game file: " + e);
        } catch (NullPointerException e) {
            logger.severe("Cannot open the requested file: " + e);
        }

        return levels;
    }

    public Level getNextLevel() {
        if (currentLevel == null) {
            return levels.get(0);
        }
        int currentLevelIndex = currentLevel.getIndex();
        if (currentLevelIndex < levels.size()) {
            return levels.get(currentLevelIndex);
        }
        gameComplete = true;
        return null;
    }
    public void resetCurrentLevel() {
        int currentLevelIndex = currentLevel.getIndex();
        steps.clear();

        try {
            currentLevel = deepGetLevel(defaultLevels.get(currentLevelIndex - 1));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (isDebugActive()){
            System.out.println("Level reset to " + currentLevelIndex);
            System.out.println("defaultLevels " + defaultLevels.toString());
            System.out.println("Recorded steps are reset.");
        }
    }
    public void undo(Level undoLevel) {
        try {
            currentLevel = deepGetLevel(undoLevel);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves game state to "saves" folder
     * - Creates directory if "saves" folder does not exist.
     * - Parse game state and write save file in correct format.
     */
    public void saveGame(){

        try {
            File directory = new File(System.getProperty("user.dir") + "/" + "saves");
            directory.mkdirs();
            BufferedWriter out = new BufferedWriter(new FileWriter(directory + "/" + GAME_NAME + ".skb"));
            out.write("MapSetName: " + GAME_NAME + "\n");

            /*TODO: 一处bug，打印不出D，*/
//            out.write("MovesCount: " + movecount + "\n");需要传入keyhandler里面的movescount

            out.write("Current level index: " + currentLevel.getIndex() + "\n");

            Level temp = currentLevel;
            for (int i = currentLevel.getIndex(); i < levels.size(); i++){
                 out.write("LevelName: " + temp.getName() + "\n");
                 out.write(temp.toString() + "\n");
                 temp = levels.get(i);
            }

            if (isDebugActive()){
                System.out.println("Game saved to " + directory + "/" + GAME_NAME + ".skb");
            }

            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}